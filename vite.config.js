import { defineConfig } from "vite";
import { resolve } from "path";

export default defineConfig({
  root: "src", // Sets the root directory for Vite, where index.html is located
  base: "/", // Specifies the base path for the application (root of the domain)
  publicDir: "public",
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, "src/index.html"),
        whoami: resolve(__dirname, "src/whoami.html"),
        gallery: resolve(__dirname, "src/gallery.html"),
      },
    },
    outDir: "../dist", // Specifies the output directory relative to the root
    emptyOutDir: true, // Ensures the output directory is cleaned before building
    target: "es2016", // Sets the JavaScript target version (ESNext)
    assetsInlineLimit: 0, // Disables asset size limit for inlining
  },
  resolve: {
    alias: {
      "@": resolve(__dirname, "src"), // Creates an alias for the src directory
    },
  },
});
